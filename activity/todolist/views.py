from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    context = { 
        'todoitem_list': todoitem_list,
        'user': request.user,
    }
    return render(request, 'todolist/index.html', context)

def todoitem(request, todoitem_id):
    object = get_object_or_404(ToDoItem, pk = todoitem_id)
    todoitem = model_to_dict(object)
    return render(request, 'todolist/todoitem.html', todoitem)

def register(request):
    template = 'todolist/register.html'
    
    context = {
        'is_registered': False,
        'is_successful': False,
        'password_different': False,
    }

    if request.method == 'GET':
        return render(request, template, context)
    
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if not form.is_valid():
            form = RegisterForm()
        else:
            context['username'] = form.cleaned_data['username']
            context['firstname'] = form.cleaned_data['firstname']
            context['lastname'] = form.cleaned_data['lastname']
            context['email'] = form.cleaned_data['email']
            context['password'] = form.cleaned_data['password']
            context['confirm_password'] = form.cleaned_data['confirm_password']

            # Check if password and confirm password match
            if context['password'] != context['confirm_password']:
                context['password_different'] = True
                return render(request, template, context)
            
            try:
                user = User.objects.get(username = context['username'])
            except User.DoesNotExist:
                user = None

            if user is not None:
                context['is_registered'] = True
                return render(request, template, context)

            user = User(
                    username = context['username'],
                    first_name = context['firstname'], 
                    last_name = context['lastname'],
                    email = context['email'],
                    is_staff =  False,
                    is_active = True
                )
            user.set_password(context['password'])
            user.save()
            context = { 'is_successful': True }
            return render(request, template, context)
    

def change_password(request):
    template = 'todolist/change_password.html'
    is_user_authenticated = False

    user = authenticate(username = 'kimminjeong', password = 'P@ssw0rd789')
    if user is not None:
        user.set_password('P@ssw0rd')
        user.save()
        is_user_authenticated = True

    context = {
        'is_authenticated': is_user_authenticated
    }
    return render(request, template, context)

def login_view(request):
    context = { 'error': False }
    if request.method == 'POST':
        
        form = LoginForm(request.POST)
        if not form.is_valid():
            form = LoginForm()
        else:
            credentials = {
                'username': form.cleaned_data['username'],
                'password': form.cleaned_data['password'],
            }

            user = authenticate(**credentials)
            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = { 'error': True }
    
    return render(request, 'todolist/login.html', context)

def logout_view(request):
    logout(request)
    return redirect('todolist:index')

def add_task(request):
    context = { 'error': False, 'user': request.user }
    if request.method == 'POST':

        form = AddTaskForm(request.POST)
        if not form.is_valid():
            form = AddTaskForm()
        else:
            task = {
                'task_name': form.cleaned_data['task_name'],
                'description': form.cleaned_data['description'],
                'date_created': timezone.now(),
            }
            
            duplicates = ToDoItem.objects.filter(task_name = task['task_name'])
            if not duplicates:
                ToDoItem.objects.create(**task, user_id = request.user.id)
                return redirect('todolist:index')
            else:
                context = { 'error': True }

    return render(request, 'todolist/add_task.html', context)

def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.get(pk = todoitem_id)
    context = { 
        'error': False, 'user': request.user,
        'todoitem_id': todoitem_id,
        'task_name': todoitem.task_name,
        'description': todoitem.description,
        'status': todoitem.status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)
        if not form.is_valid():
            form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem.task_name = task_name
                todoitem.description = description
                todoitem.status = status
                todoitem.save()
                return redirect('todolist:index')
            else:
                context = {
                    'error': True
                }

    return render(request, 'todolist/update_task.html', context)

def delete_task(request, todoitem_id):
    ToDoItem.objects.filter(pk = todoitem_id).delete()
    return redirect('todolist:index')
